<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ClienteController extends Controller {
    public function index() {
        //$clientes = Cliente::all(); // Mostrar todos los clientes en un array
        $clientes = Cliente::paginate(12);

        return view('cliente.index', ['clientes' => $clientes]);
    }

    public function show($id) {
        $cliente = Cliente::find($id);

        return view('cliente.show', ["cliente" => $cliente]);
    }

    // nos carga el formulario para crear un nuevo registro
    public function create() {
        return view('cliente.create');
    }

    public function store(Request $request) {
        $request->validate([
            'nombre' => 'required',
            'email' => 'required',
            'telefono' => 'required'
        ]);

        // Asignacion masiva
        $cliente = Cliente::create($request->all());

        // Sin asignacion masiva
        // $cliente = new Cliente();
        //     $cliente->nombre = $request->input('nombre');
        //     $cliente->email = $request->input('email');
        //     $cliente->telefono = $request->input('telefono');
        // $cliente->save();

        // Redireccionar a la vista show
        return redirect()
            ->route('cliente.show', $cliente->id)
            ->with('mensaje', 'Cliente creado con éxito')
        ; 
    }

    /**
     * Cargar formulario para editar
     */
    public function edit($id) {
        $cliente = Cliente::find($id);

        return view('cliente.edit', ['cliente' => $cliente]);
    }

    /**
     * Cargar formulario para editar cuando pulse el bonton de actualizar
    */
    public function update(Request $request, Cliente $cliente) {
        $request->validate([
            'nombre' => 'required',
            'email' => 'required',
            'telefono' => 'required'
        ]);
        
        $cliente->update($request->all());

        return redirect()
            ->route('cliente.show', $cliente->id)
            ->with('mensaje', 'Cliente actualizado con éxito')
        ;
    }

    public function destroy(Cliente $cliente) {
        $cliente->delete();
        
        return redirect()
            ->route('cliente.index')
            ->with('mensaje', 'Cliente borrado con éxito')
        ;
    }
}