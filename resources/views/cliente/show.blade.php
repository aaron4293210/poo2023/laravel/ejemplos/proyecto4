@extends('layouts.main')

@section('titulo', 'Mostrar')

@section('contenido')
<div class="row">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active disabled" aria-disabled="true">Cliente {{ $cliente->id }}</a>
        </li>
    
        <li class="nav-item">
            <a class="nav-link" href="{{ route('cliente.edit', $cliente->id) }}">Editar Cliente</a>
        </li>
        
        <li class="nav-item" style="margin-left: auto;">
            <a class="nav-link" aria-current="page" href="{{ route('cliente.index') }}">Volver</a>
        </li>
    </ul>

    @if (session('mensaje'))
        <div class="alert alert-success mt-4 text-center" role="alert">
            {{ session('mensaje') }}
        </div>
    @endif
    
    <div class="col-lg-8 my-4 mb-sm-0 offset-lg-2">
        <div class="card text-center listadoTarjetaCliente">
            <div class="card-header titulo d-flex justify-content-between align-items-center">
                <span class="me-auto">Cliente {{ $cliente->id }}</span>

                <form action="{{ route('cliente.destroy', $cliente)}}" method="POST" class="ms-auto">
                    @csrf @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger border-0" data-bs-toggle="tooltip" title="Eliminar cliente"><i class="fas fa-trash"></i></button>
                </form>      
            </div>

            <div class="card-body">
                <i class="fa-solid fa-user-astronaut fa-5x profile-pic color-blue"></i>
                <h4 class="card-title font-italic nombreListadoCliente d-flex justify-content-center align-items-center">{{ $cliente->nombre }}</h4>

                <ul class="list-group list-group-flush mb-3">
                    <li class="list-group-item list-group-item-action">
                        <div class="row">
                            <div class="col-6">
                                Email
                            </div>

                            <div class="col-6">
                                {{ $cliente->email }}
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item list-group-item-action">
                        <div class="row">
                            <div class="col-6">
                                Telefono
                            </div>

                            <div class="col-6">
                                {{ $cliente->telefono }}
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection