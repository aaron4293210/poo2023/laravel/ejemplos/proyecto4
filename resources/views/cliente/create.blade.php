@extends('layouts.main')

@section('titulo', 'Crear Clientes')

@section('cabecera')
    @parent
@endsection

@section('contenido')
    <h1 class="display-6 text-center">Crear Nuevo Cliente</h1>

    <div class="row justify-content-center">
        <div class="col-lg-8">
            <form  action="{{ route('cliente.store') }}" class="card-body cardbody-color p-lg-5 needs-validation" novalidate method="POST">
                @csrf

                <div class="input-group mb-3">
                    <label for="nombre" class="form-label">Nombre Completo<span class="text-danger">*</span></label>
                    
                    <div class="input-group has-validation">
                        <span class="input-group-text"><i class="fa-solid fa-user-astronaut"></i></span>
                        
                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre Completo" required>
                        <div class="invalid-feedback">Por favor introduzca su nombre</div>
                    </div>
                </div>
                
                <div class="input-group mb-3">
                    <label for="email" class="form-label">Correo<span class="text-danger">*</span></label>
                    
                    <div class="input-group has-validation">
                        <span class="input-group-text"><i class="fa-regular fa-envelope"></i></span>

                        <input type="text" class="form-control" id="email" name="email" placeholder="Correo" required>
                        <div class="invalid-feedback">Por favor introduzca su correo</div>
                    </div>
                </div>

                <div class="input-group mb-3">
                    <label for="telefono" class="form-label">Teléfono<span class="text-danger">*</span></label>
                    
                    <div class="input-group has-validation">
                        <span class="input-group-text"><i class="fa-solid fa-mobile-screen-button"></i></span>

                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" required>
                        <div class="invalid-feedback">Por favor introduzca su teléfono</div>
                    </div>
                </div>
    
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">Insertar</button>
                </div>
            </form>
        </div>
    </div>

    <script>
        document.addEventListener('submit', validacionFormulario);

        function validacionFormulario(e) {
            const formulario = e.target;

            if (!formulario.classList.contains('needs-validation')) {
                return;
            }

            // Si el formulario no es válido, previene el evento de envío y detiene la propagación
            if (!formulario.checkValidity()) {
                e.preventDefault();
                e.stopPropagation();
            }

            formulario.classList.add('was-validated');
        }
    </script>
@endsection
