@extends('layouts.main')

@section('titulo', 'Editar Cliente')

@section('cabecera')
    @parent
@endsection

@section('contenido')
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="{{ route('cliente.show', $cliente->id) }}">Cliente {{ $cliente->id }}</a>
        </li>

        <li class="nav-item">
            <a class="nav-link active disabled" aria-disabled="true" href="{{ route('cliente.edit', $cliente->id) }}">Editar Cliente</a>
        </li>
        
        <li class="nav-item" style="margin-left: auto;">
            <a class="nav-link" aria-current="page" href="{{ route('cliente.show', $cliente->id) }}">Volver</a>
        </li>
    </ul>

    @if (session('mensaje'))
        <div class="alert alert-success mt-4 text-center" role="alert">
            {{ session('mensaje') }}
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-lg-8">
            <form  action="{{ route('cliente.update', $cliente) }}" class="card-body cardbody-color p-lg-5 needs-validation" novalidate method="POST">
                @csrf
                @method('PUT')

                <div class="input-group mb-3">
                    <label for="nombre" class="form-label">Nombre Completo</label>
                    
                    <div class="input-group has-validation">
                        <span class="input-group-text"><i class="fa-solid fa-user-astronaut"></i></span>
                        
                        <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $cliente->nombre }}" required>
                        <div class="invalid-feedback">Por favor introduzca su nombre</div>
                    </div>
                </div>
                
                <div class="input-group mb-3">
                    <label for="email" class="form-label">Correo</label>
                    
                    <div class="input-group has-validation">
                        <span class="input-group-text"><i class="fa-regular fa-envelope"></i></span>

                        <input type="text" class="form-control" id="email" name="email" value="{{ $cliente->email }}" required>
                        <div class="invalid-feedback">Por favor introduzca su correo</div>
                    </div>
                </div>

                <div class="input-group mb-3">
                    <label for="telefono" class="form-label">Teléfono</label>
                    
                    <div class="input-group has-validation">
                        <span class="input-group-text"><i class="fa-solid fa-mobile-screen-button"></i></span>

                        <input type="text" class="form-control" id="telefono" name="telefono" value="{{ $cliente->telefono }}" required>
                        <div class="invalid-feedback">Por favor introduzca su teléfono</div>
                    </div>
                </div>
    
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                </div>
            </form>
        </div>
    </div>

    <script>
        document.addEventListener('submit', validacionFormulario);

        function validacionFormulario(e) {
            const formulario = e.target;

            if (!formulario.classList.contains('needs-validation')) {
                return;
            }

            // Si el formulario no es válido, previene el evento de envío y detiene la propagación
            if (!formulario.checkValidity()) {
                e.preventDefault();
                e.stopPropagation();
            }

            formulario.classList.add('was-validated');
        }
    </script>
@endsection
