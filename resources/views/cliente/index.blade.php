@extends('layouts.main')

@section('titulo', 'Clientes')

@section('contenido')
<h1 class="display-5 text-center">Listar Clientes</h1>

@if (session('mensaje'))
    <div class="alert alert-success mt-4 text-center" role="alert">
        {{ session('mensaje') }}
    </div>
@endif

<div class="row">
    @foreach ($clientes as $cliente)
        <div class="col-lg-3 my-4 mb-sm-0">
            <div class="card text-center listadoTarjetaCliente border-0 card-box-shadow">
                <div class="card-header titulo mb-3">
                    Cliente {{ $cliente->id }}
                </div>

                <a href="{{ route('cliente.show', $cliente->id) }}"><i class="fa-solid fa-user-astronaut fa-3x profile-pic color-blue"></i></a>
                
                <div class="card-body">
                    <h4 class="card-title font-italic nombreListadoCliente d-flex justify-content-center align-items-center">{{ $cliente->nombre }}</h4>

                    <div class="btn-group" >
                        <a href="{{ route('cliente.edit', $cliente->id) }}" class="btn btn-outline-success mx-1">Editar</a>
                        <form action="{{ route('cliente.destroy', $cliente)}}" method="POST">
                            @csrf @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger mx-1">Borrar</button>
                        </form>
                    </div>
                </div>

                <div class="card-footer text-muted ">
                    <div class="row">
                        <div class="col-4 border-end">
                            <a href="mailto:{{ $cliente->email }}" class="link-primary"><i class="fa-regular fa-envelope"></i></a>
                        </div>
                        
                        <div class="col-4 border-end">
                            <a href="tel:{{ $cliente->telefono }}" class="link-primary"><i class="fa-solid fa-mobile-screen-button"></i></a>
                        </div>

                        <div class="col-4">
                            <a href="{{ route('cliente.show', $cliente->id) }}" class="link-primary"><i class="fa-regular fa-address-card"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="row mt-3">
    {{ $clientes->links() }}
</div>
@endsection