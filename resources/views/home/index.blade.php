@extends('layouts.main')

@section('titulo', 'Inicio')

@section('cabecera')
    @parent
@endsection

@section('contenido')
    <div class="container my-5">
        <div class="row my-5">
            <div class="col-lg-6 p-5">
                <h1 class="display-5">Pagina de Inicio</h1>

                <figure class="blockquote">
                    <blockquote class="blockquote">
                        <p>A well-known quote, contained in a blockquote element.</p>
                    </blockquote>
                    <figcaption class="blockquote-footer">
                        Someone famous in <cite title="Source Title">Source Title</cite>
                    </figcaption>
                </figure>

                <button class="btn btn-primary">Mostrar Clientes</button>
            </div>
            <div class="col-lg-6 p-5">
                <img src="{{ asset('imgs/landingPage/landing_page.svg') }}" class="img-fluid" alt="Landing Page Image">
            </div>
        </div>

        <div class="row row-cols-1 row-cols-md-3 g-4 text-center">
            <div class="col d-flex">
                <div class="card border-0 flex-fill">
                    <img src="{{ asset('imgs/landingPage/about.svg') }}" class="card-img-top img-fluid"
                        style="object-fit: contain; height: 200px;" alt="about">
                    <div class="card-body">
                        <h5 class="card-title">Quienes Somos</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to
                            additional content. This content is a little bit longer.</p>
                        <a class="btn btn-outline-primary" href="#" role="button">Quienes Somos</a>
                    </div>
                </div>
            </div>

            <div class="col d-flex">
                <div class="card border-0 flex-fill">
                    <img src="{{ asset('imgs/landingPage/services.svg') }}" class="card-img-top img-fluid"
                        style="object-fit: contain; height: 200px;" alt="services">
                    <div class="card-body">
                        <h5 class="card-title">Servicios</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to
                            additional content. This content is a little bit longer.</p>
                        <a class="btn btn-outline-primary" href="#" role="button">Servicios</a>
                    </div>
                </div>
            </div>

            <div class="col d-flex">
                <div class="card border-0 flex-fill">
                    <img src="{{ asset('imgs/landingPage/login.svg') }}" class="card-img-top img-fluid"
                        style="object-fit: contain; height: 200px;" alt="login">
                    <div class="card-body">
                        <h5 class="card-title">Iniciar Sesión</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to
                            additional content.</p>
                        <a class="btn btn-outline-primary" href="#" role="button">Iniciar Sesión</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
