<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('home.index') }}"><h1 class="display-5 tituloMenu">Aplicacion 4</h1></a>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ Request::routeIs('home.index') ? ' active' : '' }}" href="{{ route('home.index') }}">Inicio</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ Request::routeIs('cliente.index') ? ' active' : '' }}" href="{{ route('cliente.index') }}">Listar Clientes</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ Request::routeIs('cliente.create') ? ' active' : '' }}" href="{{ route('cliente.create') }}">Insertar Clientes</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
